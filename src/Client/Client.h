#pragma once

#include "Server/Server.h"

typedef struct {
    serverStruct* serverContext;
    int socket;
} clientContext;

void clientStart(char* username, char* address);
