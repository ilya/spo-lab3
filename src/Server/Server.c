#include "Server.h"
#include <sys/socket.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdatomic.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include "Parser/Parser.h"
#include "main.h"
#include "ClientHandling.h"

#include "Client/Client.h"


void* serverMainThreadWorker(void *serverContextTemplate){
    serverStruct* context = ((serverStruct*)serverContextTemplate);

    bool mainWorkerShouldStop = false;

    // Starting up socket
    int serverSocket;
    if ((serverSocket = (socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0))) == -1){
        perror("Server couldn't start");
        context->serverShouldStop = true;
        pthread_exit(NULL);
    }

    struct sockaddr_in sockAddr;
    sockAddr.sin_family = AF_INET;
    sockAddr.sin_addr.s_addr =INADDR_ANY;
    sockAddr.sin_port = htons(PORT);

    //bind server socket
    if ((bind(serverSocket, (struct sockaddr*)&sockAddr, sizeof(sockAddr))) < 0){
        perror("Error binding socket");
        context->serverShouldStop = true;
        pthread_exit(NULL);
    }

    listen(serverSocket, 255);

    while (!context->serverShouldStop){
        int ret = accept(serverSocket, NULL, NULL);

        if (ret >0){
            handleClient(context,ret);
        }
        //Don't burn the cpu
        sched_yield();
    }
    puts("We are shutting down...");

    for (int i=0; i<context->clients.amount; i++){
        close(context->clients.sockets[i]);
    }
    close(serverSocket);
    pthread_exit(NULL);
}

//NOT A CONSTRUCTOR!!!!
//it just looks like it's a constructor
serverStruct* initServerContext(){
    serverStruct* context = malloc(sizeof(serverStruct));
    context->serverShouldStop = false;
    context->clients.amount = 0;
    context->clients.capacity = 2;
    context->clients.sockets = malloc(sizeof(int)*2);
    context->prev_id = 0;
    context->messages = NULL;

    return context;
}

void serverStart(){
    serverStruct* context = initServerContext();


    pthread_t serverThread;
    pthread_create(&serverThread,NULL,&serverMainThreadWorker,(void*)context);
    //Something with SIGPIPE
    {
        //include signal.h
    }

    char* line = NULL;
    size_t bufferLength = 0;

    while (!context->serverShouldStop){
        printf("> ");
        size_t charsNum = getline(&line,&bufferLength, stdin);
        if (charsNum == (size_t)-1){ //? I am not sure about that
            free(line);
            puts("Server got EOF. Abort.");
            return;
        }
        char** args = NULL;
        int amountOfWords = parseCommand(line, &args);
        if (amountOfWords == 0){
            //free(line);
            if (args != NULL){
                free(args);
            }
            continue;
        }
        if (strcmp("exit",args[0]) == 0){
            context->serverShouldStop = true;
        }
    }
    puts("Server stopped. Turning off");
    pthread_join(serverThread,NULL);
}
